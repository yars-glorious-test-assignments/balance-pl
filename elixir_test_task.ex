# Тестовое задание на позицию Elixir разработчика Balance-Platform


# Есть справочник курсов валют,
# записанный в атрибут @currencies
# Необходимо добавить функцию exchange, чтобы она
# могла переводить валюты из одну в другую с помощью рубля

defmodule Currency do
  @currencies %{
    # Доллар
    usd: %{amount: 1, rub: 72},
    # Евро
    euro: %{amount: 1, rub: 80},
    # Польский Злотый
    pln: %{amount: 1, rub: 20},
    # Японская иена
    jpy: %{amount: 100, rub: 70}
  }

  @subunit_precisions %{
    # Доллар
    usd: 2,
    # Евро
    euro: 2,
    # Польский Злотый
    pln: 2,
    # Японская иена
    jpy: 0,
    # Российский деревянный
    rub: 2,
  }

  # Основная функция, которую требуется написать
  def exchange(amount_from, from_currency, to_currency) do
    {from_amount, from_rub} = get_currency_data(from_currency)
    {to_amount, to_rub} = get_currency_data(to_currency)
    subunit_factor = get_subunit_factor(to_currency)

    subunit_factor * amount_from * from_rub * to_amount
    |> div(to_rub * from_amount)
    |> format_money(subunit_factor)
  end

  # Вспомогательная функция, которую нужно использовать
  defp get_currency_data(:rub), do: {1, 1}
  defp get_currency_data(title) do
    %{amount: amount, rub: rub} = @currencies[title]
    {amount, rub}
  end

  defp get_precision_data(currency) do
    @subunit_precisions[currency]
  end

  defp get_subunit_factor(currency), do: Integer.pow(10, get_precision_data(currency))

  defp format_money(money, presicion), do: {
    div(money, presicion),
    rem(money, presicion)
  }
end

# Это тесты "на коленке", нужно поочередно раскомментировать строки IO.inspect и проверять результат
IO.inspect(Currency.exchange(1, :euro, :pln))  # ~ 4
IO.inspect(Currency.exchange(1000, :jpy, :usd))  # ~ 9.7
IO.inspect(Currency.exchange(1000, :usd, :jpy))
IO.inspect(Currency.exchange(10, :euro, :rub))  # ~ 800
IO.inspect(Currency.exchange(12341234123, :rub, :rub)) # exactly 12341234123

# Для прохождения этого теста существует несколько вариантов решений, нужно реализовать в коде один из возможных.
# Будет плюсом, если опишешь дополнительно альтернативные решения

## Комментарий по решению:
## Считать деньги использую математику с плавающей точкой - очень плохая идея, вспоминается мем:
## "If I had a dime for every time I've seen someone use FLOAT to store currency, I'd have $123.35233404

## Если в системе валюта одна, то можно хранить ее как целое число, сдвинутое на два десятичных разряда влево, т.е. $100 => 100_00 в целочисленной переменной.
##
## Если валют несколько, то можно выделить в отдельный тип Money и хранить целочисленно количество валюты, количество сабвалюты(если имеется)
## и применять целочисленную математику при обработке, благо в эликсире целые числа не ограничены по размеру
